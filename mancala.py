#!/usr/bin/env python3

from copy import deepcopy

class Mancala:
	def __init__(self):
		self.score = [0, 0]
		self.board = [
			[4, 4, 4, 4, 4, 4],
			[4, 4, 4, 4, 4, 4],
		]
		self.board_side_length = len(self.board[0])

	def __str__(self):
		score_header_template = (
			'+===========+\n'
			'|{:^11}|\n'
			'+===========+\n'
		)

		player_one_score_header = score_header_template.format(self.get_score(0))
		player_two_score_header = score_header_template.format(self.get_score(1))

		pit_rows = ''
		pit_row_template = '|{:^5}|{:^5}|\n'

		for i in range(self.board_side_length):
			left_pit_seeds = self.get_num_seeds(0, i)
			right_pit_seeds = self.get_num_seeds(1, (self.board_side_length-1)-i)

			pit_rows += pit_row_template.format(left_pit_seeds, right_pit_seeds)

		return player_two_score_header + pit_rows + player_one_score_header

	def get_score(self, player_number):
		return self.score[player_number]

	def get_utility(self):
		# Positive utility = Player 0 is in the lead
		# Zero = Neither player is in the lead
		# Negative utility = Player 1 is in the lead
		return self.get_score(player_number=0) - self.get_score(player_number=1)

	def get_max_possible_score(self):
		player_one_score = self.get_score(0)
		player_two_score = self.get_score(1)
		seeds_remaining = sum(self.board[0]) + sum(self.board[1])

		return player_one_score + player_two_score + seeds_remaining

	def get_num_seeds(self, side_number, pit_number):
		pit_number_min = 0
		pit_number_max = self.board_side_length - 1

		if not pit_number_min <= pit_number <= pit_number_max:
			raise IndexError(
				'pit_number is out of range. It must be an integer between {} and {} (both inclusive)'.format(
					pit_number_min,
					pit_number_max,
				)
			)

		return self.board[side_number][pit_number]

	def set_num_seeds(self, side_number, pit_number, num_seeds):
		pit_number_min = 0
		pit_number_max = self.board_side_length - 1

		if not pit_number_min <= pit_number <= pit_number_max:
			raise IndexError(
				'pit_number is out of range. It must be an integer between {} and {} (both inclusive)'.format(
					pit_number_min,
					pit_number_max,
				)
			)

		if num_seeds < 0:
			raise ValueError('num_seeds must be a non-negative integer')

		self.board[side_number][pit_number] = num_seeds

	def increment_num_seeds(self, side_number, pit_number):
		self.board[side_number][pit_number] += 1

	def increment_score(self, player_number):
		self.score[player_number] += 1

	def get_non_empty_pit_indices(self, player_number):
		return [index for index,num_seeds in enumerate(self.board[player_number]) if num_seeds > 0]

	def is_game_over(self):
		# Game over if all pits on one side have no seeds inside
		return any(all(num_seeds == 0 for num_seeds in side) for side in self.board)

	def choose(self, player_number, current_side, pit_number):
		num_seeds = self.get_num_seeds(current_side, pit_number)
		self.set_num_seeds(current_side, pit_number, 0)

		next_player_number = not player_number

		if num_seeds == 0:
			raise ValueError('Only non-empty pits can be chosen')

		while num_seeds > 0:
			pit_number += 1
			num_seeds -= 1

			if pit_number == self.board_side_length:
				if current_side == player_number:
					self.increment_score(player_number)

					if num_seeds == 0:
						# Last seed ended in store - player gets to go again
						return player_number
				else:
					num_seeds += 1

				# Reset to first pit on the other side
				pit_number = -1
				current_side = not current_side
			else:
				self.increment_num_seeds(current_side, pit_number)

		# Continue if the last seed has been dropped and the pit is non-empty
		if self.get_num_seeds(current_side, pit_number) > 1:
			next_player_number = self.choose(player_number, current_side, pit_number)

		return next_player_number

	def test(self):
		def test_avalanche():
			mancala = Mancala()

			mancala.score = [0, 0]
			mancala.board = [
				[4, 4, 4, 4, 4, 4],
				[4, 4, 4, 4, 4, 4],
			]

			next_player = mancala.choose(0, 0, 0)

			expected_score = [5, 0]
			expected_board = [
				[1, 4, 1, 10, 1, 1],
				[3, 0, 2, 9, 2, 9],
			]
			expected_next_player = 1

			assert mancala.score == expected_score
			assert mancala.board == expected_board
			assert next_player == expected_next_player

		def test_free_turn():
			mancala = Mancala()

			mancala.score = [0, 0]
			mancala.board = [
				[4, 4, 4, 4, 4, 4],
				[4, 4, 4, 4, 4, 4],
			]

			next_player = mancala.choose(0, 0, 2)

			expected_score = [1, 0]
			expected_board = [
				[4, 4, 0, 5, 5, 5],
				[4, 4, 4, 4, 4, 4],
			]
			expected_next_player = 0

			assert mancala.score == expected_score
			assert mancala.board == expected_board
			assert next_player == expected_next_player

			next_player = mancala.choose(0, 0, 0)

			expected_score = [2, 0]
			expected_board = [
				[1, 6, 0, 7, 1, 6],
				[5, 5, 5, 0, 5, 5],
			]
			expected_next_player = 1

			assert mancala.score == expected_score
			assert mancala.board == expected_board
			assert next_player == expected_next_player

		def test_game_over():
			mancala = Mancala()

			mancala.score = [40, 5]
			mancala.board = [
				[0, 0, 0, 0, 0],
				[1, 0, 2, 0, 0],
			]

			assert mancala.is_game_over() == True

			mancala.score = [39, 5]
			mancala.board = [
				[1, 0, 0, 0, 0],
				[1, 0, 2, 0, 0],
			]

			assert mancala.is_game_over() == False

		def test_opponent_move():
			mancala = Mancala()

			mancala.score = [5, 0]
			mancala.board = [
				[1, 4, 1, 10, 1, 1],
				[3, 0, 2, 9, 2, 9],
			]

			next_player = mancala.choose(1, 1, 0)

			expected_score = [5, 1]
			expected_board = [
				[2, 5, 2, 11, 2, 2],
				[1, 1, 3, 0, 3, 10],
			]
			expected_next_player = 0

			assert mancala.score == expected_score
			assert mancala.board == expected_board
			assert next_player == expected_next_player

		test_avalanche()
		test_free_turn()
		test_game_over()
		test_opponent_move()


def minimax(mancala_orig, max_depth, player_number=0):
	utilities = []

	if player_number == 0:
		opponent_function = minimize
		player_function = maximize
		best_utility_function = max
	elif player_number == 1:
		opponent_function = maximize
		player_function = minimize
		best_utility_function = min
	else:
		raise ValueError('player_number must be 0 or 1')

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)
		depth = 1

		if player_number_next == player_number:
			utility = player_function(mancala, max_depth, depth)
		else:
			utility = opponent_function(mancala, max_depth, depth + 1)

		utilities.append((pit_number, utility))

	print(utilities)

	# Find best utility based on second element
	return best_utility_function(utilities, key = lambda scores: scores[1])

def maximize(mancala_orig, max_depth, depth):
	if mancala_orig.is_game_over() or depth > max_depth:
		return mancala_orig.get_utility()

	player_number = 0
	utilities = []

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)

		if player_number_next == player_number:
			utility = maximize(mancala, max_depth, depth)
		else:
			utility = minimize(mancala, max_depth, depth + 1)

		utilities.append(utility)

	return max(utilities)

def minimize(mancala_orig, max_depth, depth):
	if mancala_orig.is_game_over() or depth > max_depth:
		return mancala_orig.get_utility()

	player_number = 1
	utilities = []

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)
		if player_number_next == player_number:
			utility = minimize(mancala, max_depth, depth)
		else:
			utility = maximize(mancala, max_depth, depth + 1)

		utilities.append(utility)

	return min(utilities)


def alpha_beta(mancala_orig, max_depth, player_number=0):
	utilities = []

	if player_number == 0:
		opponent_function = alpha_beta_minimize
		player_function = alpha_beta_maximize
		best_utility_function = max
	elif player_number == 1:
		opponent_function = alpha_beta_maximize
		player_function = alpha_beta_minimize
		best_utility_function = min
	else:
		raise ValueError('player_number must be 0 or 1')

	alpha = -mancala_orig.get_max_possible_score()
	beta = mancala_orig.get_max_possible_score()

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)
		depth = 1

		if player_number_next == player_number:
			utility = player_function(mancala, max_depth, depth, alpha, beta)
		else:
			utility = opponent_function(mancala, max_depth, depth + 1, alpha, beta)

		if player_number == 0 and utility >= beta:
			return (pit_number, utility)
		elif player_number == 1 and utility <= alpha:
			return (pit_number, utility)

		utilities.append((pit_number, utility))

		if player_number == 0:
			alpha = best_utility_function(utilities, key=lambda x:x[1])[1]
		else:
			beta = best_utility_function(utilities, key=lambda x:x[1])[1]

	print(utilities)

	# Find best utility based on second element
	return best_utility_function(utilities, key = lambda scores: scores[1])

def alpha_beta_maximize(mancala_orig, max_depth, depth, alpha, beta):
	if mancala_orig.is_game_over() or depth > max_depth:
		return mancala_orig.get_utility()

	player_number = 0
	utilities = [-mancala_orig.get_max_possible_score()]

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)

		if player_number_next == player_number:
			utility = alpha_beta_maximize(mancala, max_depth, depth, alpha, beta)
		else:
			utility = alpha_beta_minimize(mancala, max_depth, depth + 1, alpha, beta)

		if utility >= beta:
			return utility

		utilities.append(utility)

		alpha = max(alpha, max(utilities))

	return max(utilities)

def alpha_beta_minimize(mancala_orig, max_depth, depth, alpha, beta):
	if mancala_orig.is_game_over() or depth > max_depth:
		return mancala_orig.get_utility()

	player_number = 1
	utilities = [mancala_orig.get_max_possible_score()]

	for pit_number in mancala_orig.get_non_empty_pit_indices(player_number):
		mancala = deepcopy(mancala_orig)

		player_number_next = mancala.choose(player_number, player_number, pit_number)

		if player_number_next == player_number:
			utility = alpha_beta_minimize(mancala, max_depth, depth, alpha, beta)
		else:
			utility = alpha_beta_maximize(mancala, max_depth, depth + 1, alpha, beta)

		if utility <= alpha:
			return utility

		utilities.append(utility)

		beta = min(beta, min(utilities))

	return min(utilities)


if __name__ == '__main__':
	mancala = Mancala()
	mancala.test()

	print(mancala)

	# Start off the game with a few moves from the first player
	mancala.choose(0, 0, 5)
	mancala.choose(0, 0, 2)
	mancala.choose(0, 0, 1)

	print(mancala)

	# Only consider even max depth so that maximizer and minimizer have same number of moves
	for max_depth in range(2, 7, 2):
		print(alpha_beta(mancala, max_depth))
		print('')